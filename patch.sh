#!/bin/bash

#apt-get install bbb-demo -y

#openssl dhparam -out /etc/nginx/ssl/dhp-4096.pem 4096

set -x

echo 1048576 > /proc/sys/fs/inotify/max_user_watches
#patchMeteorFile="./settings_copy.yml"
patchMeteorFile="/usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml"
replace=(
	"s/clientTitle:.*/clientTitle: Vicon/"
	's/copyright:.*/copyright: "Vicon - \\u0421\\u043e\\u0432\\u0440\\u0435\\u043c\\u0435\\u043d\\u043d\\u0430\\u044f \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430 \\u0434\\u043b\\u044f \\u0441\\u043e\\u0432\\u043c\\u0435\\u0441\\u0442\\u043d\\u043e\\u0439 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b"/'
	"s/html5ClientBuild:.*/html5ClientBuild: 2.056.116/"
	"s/helpLink:.*/helpLink: https:\/\/vicon.onoi.kg\//"
	"s/allbackLocale:.*/allbackLocale: ru-RU/"
	"s/cdn:.*/cdn: \"\"/"
	"s/showHelpButton:.*/showHelpButton: false/"
	"s/skipCheсk:.*/skipCheсk: true/"
	"s/wsUrl: ws:/wsUrl: wss:/"
	"s/url: http:/url: https:/"
	)
for index in ${!replace[*]}
do
	sed -i "${replace[$index]}" $patchMeteorFile
done

patchBBBFile="/usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties"
replace=(
	"s/bigbluebutton.web.serverURL=http:/bigbluebutton.web.serverURL=https:/"
	's/defaultWelcomeMessage=.*/defaultWelcomeMessage=\\u0414\\u043e\\u0431\\u0440\\u043e \\u043f\\u043e\\u0436\\u0430\\u043b\\u043e\\u0432\\u0430\\u0442\\u044c <b>%%CONFNAME%%<\/b>!./'
	"s/defaultWelcomeMessageFooter=.*/defaultWelcomeMessageFooter=/"
	)
for index in ${!replace[*]}
do
	sed -i "${replace[$index]}" $patchBBBFile
done

screenshareProperties="/usr/share/red5/webapps/screenshare/WEB-INF/screenshare.properties"
sed -i "s/jnlpUrl=http:/jnlpUrl=https:/" $screenshareProperties
sed -i "s/jnlpFile=http:/jnlpFile=https:/" $screenshareProperties

bbbClient="/var/www/bigbluebutton/client/conf/config.xml"
sed -e 's|http://|https://|g' -i $bbbClient

bbbDemo="/var/lib/tomcat7/webapps/demo/bbb_api_conf.jsp"
sed -e 's/http:/https:/' -i $bbbDemo

nginxSipConf="/etc/bigbluebutton/nginx/sip.nginx"
sed -i -E "s|proxy_pass http://([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}):[0-9]{1,5};|proxy_pass https://\1:7443;|g" /etc/bigbluebutton/nginx/sip.nginx

demoLanguges="/usr/share/meteor/bundle/programs/server/assets/app/locales/*"
sed -i "s/BigBlueButton/Vicon/" $demoLanguges

cp -f ./meteor/head.html /usr/share/meteor/bundle/programs/web.browser/
cp -R -f ./images /var/www/bigbluebutton-default
cp -f ./images/favicon.png /var/www/bigbluebutton-default/favicon.ico

webAppsPath="/var/lib/tomcat7/webapps/demo/*"
webAppsDemoFile="/var/lib/tomcat7/webapps/demo/demoHTML5.jsp"
webAppsDemoFile="./demoHTML5.jsp"
sed -i "s/<p>You must have the BigBlueButton HTML5 client installed to use this API demo\.<\/p>/<p><\/p>/" $webAppsDemoFile

favicon='\t<link rel="icon" href="\/favicon.png">'
css='\t<link rel="stylesheet" href="\/css\/style.css">'
sed -i -E "/<\/title>/{N;N;s/(<\/title>.*)\n(<\/head>)/\1\n${favicon}\n${css}\n\2/}" $webAppsDemoFile

cp ./files/default.pdf  /opt/scripts/bigbluebutton/bigbluebutton-config/web/
cp ./files/default.pdf  /opt/scripts/bigbluebutton/bigbluebutton-config/slides/blank-presentation.pdf
cp ./files/default.pdf  /var/www/bigbluebutton-default/

git clone https://github.com/access-news/freeswitch-sounds.git
cp -r ./freeswitch-sounds/ru /opt/freeswitch/share/freeswitch/sounds
rm -rf /opt/freeswitch/share/freeswitch/sounds/ru/RU/vika/
mv /opt/freeswitch/share/freeswitch/sounds/ru/RU/elena/conference/44000 /opt/freeswitch/share/freeswitch/sounds/ru/RU/elena/conference/48000
rm -rf ./freeswitch-sounds

patchFreeswitchLangSoundsFile="/opt/freeswitch/conf/autoload_configs/conference.conf.xml"
#patchFreeswitchLangSoundsFile="./conference.conf.xml"
replace=(
	'/<profile name="default">/{N;N;s/(<profile name="default">\n)(\s*)(<!--)/\1\2<param name="sound-prefix" value="\$\${sounds_dir}\/ru\/RU\/elena\/"\/>\n\2\3/}'
	'/<profile name="wideband">/{N;N;s/(<profile name="wideband">\n)(\s*)(<param name="domain)/\1\2<param name="sound-prefix" value="\$\${sounds_dir}\/ru\/RU\/elena\/"\/>\n\2\3/}'
	'/<profile name="ultrawideband\">/{N;N;s/(<profile name="ultrawideband">\n)(\s*)(<param name="domain)/\1\2<param name="sound-prefix" value="\$\${sounds_dir}\/ru\/RU\/elena\/"\/>\n\2\3/}'
	'/<profile name="cdquality">/{N;N;s/(<profile name="cdquality">\n)(\s*)(<param name="domain)/\1\2<param name="sound-prefix" value="\$\${sounds_dir}\/ru\/RU\/elena\/"\/>\n\2\3/}'
	'/<profile name="video-mcu-stereo">/{N;N;s/(<profile name="video-mcu-stereo">\n)(\s*)(<param name="domain)/\1\2<param name="sound-prefix" value="\$\${sounds_dir}\/ru\/RU\/elena\/"\/>\n\2\3/}'
	)
for index in ${!replace[*]}
do
	sed -i -E "${replace[$index]}" $patchFreeswitchLangSoundsFile
	echo ""
done

bbb-conf --restart
